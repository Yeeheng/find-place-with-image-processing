import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { HTTP } from '@ionic-native/http';
import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  myphoto: any;
  myphoto1: any
  place : string;
  link : string;
  constructor(public navCtrl: NavController,
    public http: HTTP,
    private camera: Camera,
    private loadingCtrl: LoadingController,
    private transfer: FileTransfer,
    private alertCtl : AlertController
  ) {

  }

  takePhoto(){
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.myphoto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }





  getImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false
    }

    this.camera.getPicture(options).then((imageData) => {

      this.myphoto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {

    });
  }
  uploadImage() {
    //Show loading
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();

    //create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();

    //random int
    var random = Math.floor(Math.random() * 100);

    //option transfer
    let options: FileUploadOptions = {
      fileKey: 'file',
      fileName: "myImage_" + random + ".jpg",
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: "image/jpeg",
      headers: {}
    }

    //file transfer action
    fileTransfer.upload(this.myphoto, 'http://206.189.157.111/api/upload/uploadPhoto.php', options)
      .then((data) => {

        var rawData = JSON.parse(data['response'])
        this.myphoto1 = "http://206.189.157.111/api/upload/img/" + ((rawData['link']));
        console.log(this.myphoto1)

        loader.dismiss();

        let loaderFinal = this.loadingCtrl.create({
          content: "กำลังประมวลผล"
        });
        loaderFinal.present();

        this.http.post('http:/206.189.157.111:8181/', { "link": this.myphoto1 }, { "Content-Type": "application/x-www-form-urlencoded" })
          .then(res => {
            loaderFinal.dismiss();
            var data = JSON.parse(res['data']);
            this.place = ((data['urlImg']['place']))
            this.link = ((data['urlImg']['data']))
            console.log(JSON.stringify(this.link))
            this.alertCtl.create({
              title : "ผลลัพธ์",
              message : JSON.stringify(data)
            }).present();
            
          })
          .catch(error => {
            loaderFinal.dismiss();
            this.alertCtl.create({
              title : "ผลลัพธ์",
              message : JSON.stringify(error)
            }).present();
            
          })
        
      }, (err) => {
        console.log(err);
        this.alertCtl.create({
          title : "ผลลัพธ์",
          message : JSON.stringify(err)
        }).present();
        loader.dismiss();
      });
  }




}
